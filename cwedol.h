//
// Created by Ulysse Le Huitouze on 9/1/23.
//

#ifndef CWEDOL_CWEDOL_H
#define CWEDOL_CWEDOL_H


#include "example/carig.h"
#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif






// You can set the maximum number of possible hook handlers with CWEDOL_MAX_HOOK_HANDLERS.
// (Default value is 8)


//
// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to plugin management, which break down into two parts:
//
//
// # Hook Handler
//
// A simple abstraction for storing dynamically-loaded hooks.
//
// Related to hook handlers in this file are:
//
// - macro CW_DECLARE_HOOK_HANDLER :: PLAIN TEXT -> variable declaration
// - macro CW_INIT_HOOK_HANDLER :: PLAIN TEXT -> integral -> variable assignation
// - macro CW_FREE_HOOK_HANDLER :: PLAIN TEXT -> void
// - func cwsetpluginname :: char* -> void
// - func cwreghookhandler :: char* -> UnboundArray* -> bool
//
//
// # Hook Implementation
//
// A simple abstraction for implementing user-defined hook's contracts.
//
// Related to hook implementations in this file are:
//
// - struct HookImpl
// - macro CW_GET_HOOK_IMPL :: PLAIN TEXT -> integral -> HookImpl
// - macro CW_GET_HOOK_PTR :: PLAIN TEXT -> type -> integral -> type instance
// - macro cwreghook :: PLAIN TEXT -> bool
//
//
// ==================================================
//










/**
 * Declares a hook handler variable, i.e. a structure able to hold several hook implementations.
 * @param HANDLER_NAME the name of the hook handler to declare
 */
#define CW_DECLARE_HOOK_HANDLER(HANDLER_NAME) UnboundArray HANDLER_NAME


/**
 * Actually creates a hook handler structure with an initial capacity.
 * Once the hook handler is no longer of any use, free it using #CW_FREE_HOOK_HANDLER.
 * @param HANDLER_NAME the name of the hook handler variable declared using #CW_DECLARE_HOOK_HANDLER
 * @param INITIAL_HOOK_CAPACITY
 */
#define CW_INIT_HOOK_HANDLER(HANDLER_NAME, INITIAL_HOOK_CAPACITY) \
    HANDLER_NAME = ca_unbound_array_of(sizeof(HookImpl), INITIAL_HOOK_CAPACITY, malloc, realloc)


/**
 * Frees a hook handler structure declared with #CW_DECLARE_HOOK_HANDLER
 * and initialized with #CW_INIT_HOOK_HANDLER,
 * making it unavailable to future calls to #CW_GET_HOOK_IMPL, to #CW_GET_HOOK_PTR
 * and to #CW_FREE_HOOK_HANDLER.
 * @param HANDLER_NAME the name of the hook handler variable to free
 */
#define CW_FREE_HOOK_HANDLER(HANDLER_NAME) ca_unbound_array_free(HANDLER_NAME, free)


/**
 * Retrieves the hook implementation at an index in a hook handler structure declared
 * with #CW_DECLARE_HOOK_HANDLER and initialized with #CW_INIT_HOOK_HANDLER.
 * @param HANDLER_NAME the name of the hook handler variable to retrieve an implementation from
 * @parma INDEX[in] [integral] the index in the hook handler structure of the implementation to retrieve
 * @return[HookImpl] the implementation at index <code>INDEX</code> in hook handler <code>HANDLER_NAME</code>
 * @see #CW_GET_HOOK_PTR to retrieve only the pointer to the implementation and discard plugin metadata
 */
#define CW_GET_HOOK_IMPL(HANDLER_NAME, INDEX) (caarrget(HookImpl, &HANDLER_NAME, INDEX))


/**
 * Retrieves the pointer to the hook implementation at an index in a hook handler structure declared
 * with #CW_DECLARE_HOOK_HANDLER and initialized with #CW_INIT_HOOK_HANDLER.
 * @param HANDLER_NAME the name of the hook handler variable to retrieve an implementation from
 * @param FUNC_PTR_TYPEDEF the type of the pointer to the hook implementation
 * @param INDEX[in] [integral] the index in the hook handler structure of the implementation to retrieve
 * @return[FUNC_PTR_TYPEDEF] the implementation's pointer at index <code>INDEX</code>
 * in hook handler <code>HANDLER_NAME</code>
 * @details equivalent to @code (FUNC_PTR_TYPEDEF) CW_GET_HOOK_IMPL(HANDLER_NAME, INDEX).hook_ptr @endcode
 * @see #CW_GET_HOOK_IMPL to retrieve the whole hook implementation structure,
 * and not just a pointer to the implementation
 */
#define CW_GET_HOOK_PTR(HANDLER_NAME, FUNC_PTR_TYPEDEF, INDEX) \
((FUNC_PTR_TYPEDEF)CW_GET_HOOK_IMPL(HANDLER_NAME, INDEX).hook_ptr)





/**
 * Represents a hook implementation,
 * i.e. a pointer to some code plus some metadata regarding
 * which plugin offered this implementation.
 */
typedef struct HookImpl {
    void *hook_ptr;
    const char *plugin_name;
} HookImpl;





/**
 * Sets current plugin name.<br>
 * Called at the start of loading a plugin.<br>
 * Note that calls to #cwreghook will register their hooks
 * under the name of the latest call to this function.
 *
 * @example @code
 *  void myPluginRegistrationFunction(const char *pluginName)
 *  {
 *      cwsetpluginname(pluginName);
 *      // load shared library and well-defined entry point
 *      Plugin plugin = loadLib(pluginName);
 *
 *      // the plugin declares all its hook implementations.
 *      // contains lots of calls to cwreghook().
 *      plugin.entryPoint();
 *  }
 *
 *  void scanForPlugins()
 *  {
 *      for all SharedLib l in PLUGIN_FOLDER
 *          myPluginRegistrationFunction(l.filename);
 *  }
 * @endcode
 * @param pluginName[in] the name of the plugin currently being loaded.
 *                  Pointer must stay valid for future calls to #cwreghook (See example above).
 */
void cwsetpluginname(const char *pluginName);


/**
 * Registers a hook handler, i.e. a data structure that will hold possibly multiple implementations
 * of a "type" of hook (all functions answering by the name <code>hookName</code>).<br>
 * Additionally, for each implementation (function pointer),
 * the corresponding plugin name will be stored.
 * @param hookName[in] the name of the hook (i.e. the name of the function definition) to register
 * @param hook_handler_ptr[in] a pointer to an unbound array of <code>HookImpl</code> structures
 * @return false if a hook handler was already registered for <code>hookName</code>
 *          (in which case it got overwritten), true otherwise
 * @see #HookImpl
 */
bool cwreghookhandler(const char *hookName, UnboundArray *hook_handler_ptr);


/**
 * Registers a hook implementation.<br>
 * Prior to any call to this function, a corresponding hook handler must have been registered
 * using #cwreghookhandler. This function will return false if no hook handler are found,
 * indicating failure.
 *
 * @param HOOK_NAME[in]  The name of the hook (i.e. the name of the function definition),
 *                  as well as the name of the function in this plugin implementing the hook.
 * @return[bool] false if no hook handler were found for <code>hookName</code>
 * (which indicated either trying to work with a non-existent hook, or a bug in the main application)
 * , true otherwise.
 */
#define cwreghook(HOOK_NAME) private_cWreGhooK(STRINGIFY(HOOK_NAME), (void*)HOOK_NAME)


/**
 * <b>Intended for internal use only. Use #cwreghook instead</b>.<br>
 * Registers a hook implementation.<br>
 * Prior to any call to this function, a corresponding hook handler must have been registered
 * using #cwreghookhandler. This function will return false if no hook handler are found,
 * indicating failure.
 *
 * @param hookName     [in] the name of the interface function to register an implementation of
 * @param hook_impl_ptr[in] a pointer to the actual implementation of a hook by a plugin
 * @return whether the operation succeeded or not.
 *       Failure can be caused by:
 *       <ul>
 *          <li>no hook handler were found for <code>hookName</code>
 *          (indicates either trying to work with a non-existent hook,
 *          or a bug in the main application).</li>
 *          <li><code>hookName</code> or <code>hook_impl_ptr</code> were found to be NULL
 *          (indicates a bug in the plugin).</li>
 *          <li>an allocation failure</li>
 *       </ul>
 * @see #cwreghook
 */
bool private_cWreGhooK(const char *hookName, void *hook_impl_ptr);









// impl

#ifdef CWEDOL_IMPLEMENTATION

#include <stdio.h>
#include <string.h>

#ifndef CWEDOL_MAX_HOOK_HANDLERS
#   define CWEDOL_MAX_HOOK_HANDLERS 8
#endif


static BoundHashMap hook_handlers_by_name = ca_bound_hashmap_static_of(
        CWEDOL_MAX_HOOK_HANDLERS, strcmp
);
static const char *currentPluginName;



void cwsetpluginname(const char *pluginName)
{
    currentPluginName = pluginName;
}



bool cwreghookhandler(const char * restrict hookName, UnboundArray * restrict hook_handler_ptr)
{
    return !cahmapput(
            &hook_handlers_by_name,
            castrhash_unsafe(0, hookName),
            (void*)hookName,
            hook_handler_ptr
    );
}



bool private_cWreGhooK(const char *hookName, void *hook_impl_ptr)
{
    if (hookName == NULL || hook_impl_ptr == NULL) {
        fprintf(
                stderr,
                "[CWEDOL ERROR] cwreghook invalid parameter."
                " Both hookName & hook_impl_ptr must be non-NULL.");
        return false;
    }

    UnboundArray *handler = cahmapget(
            &hook_handlers_by_name,
            castrhash_unsafe(0, hookName),
            (void*)hookName
    );

    if (!handler)
        return false;

#if DEBUG
    if (!currentPluginName) {
        fprintf(stderr,
                "[CWEDOL ERROR] Trying to register a hook implementation"
                " while currentPluginName is NULL.\n"
                "The plugin manager must call cwsetpluginname(char*)"
                " before having a plugin register its hooks. Param hookName: '%s'", hookName);
        exit(1);    // 100% sure it's bug from main app
    }


    // several hooks from 1 plugin for same hook handler warning check

    for (uint32_t i = 0; i < handler->count; i++)
        if (strncmp( caarrget(HookImpl, handler, i).plugin_name, currentPluginName, 100 ) == 0)
            fprintf(
                    stderr,
                    "[CWEDOL WARNING] Hook handler for hook name '%s'"
                    " already contains an implementation given by plugin '%s'."
                    " Possible erroneous duplicate registration.",
                    hookName,
                    currentPluginName);


#endif //DEBUG


    // currentPluginName assumed non-NULL

    void *mem = caarrpush(
            HookImpl,
            handler,
            ((HookImpl){
                .hook_ptr = hook_impl_ptr,
                .plugin_name = currentPluginName
            })
    );
    if (mem) {
        free(mem);
        fprintf(
                stderr,
                "[CWEDOL ERROR] caarrpush allocation failed"
                " for plugin name '%s' and hook name '%s'. Terminating",
                currentPluginName,
                hookName);
        return false;
    }

    return true;
}




#endif //CWEDOL_IMPLEMENTATION


#endif //CWEDOL_CWEDOL_H
