# cwedol

## Abstract

A toy library for my toy projects.

Written in GNU-C17 (extension used is only Statement Expressions in `example/main.c`).

Features:

- Plugin Manager abstraction
- Plugin entry point and hook registration abstraction

## Design

cwedol is comprised of only two files: `cwedol.h` and `cwplugin.h`.

`cwedol.h` defines everything except the plugin entry point function definition, which is defined by `cwplugin.h`.

The basic idea of cwedol is that a main application will load at runtime arbitrary plugins, calling their entry point.
Each plugin, in their entry point, will in turn call the application back to register pointers to the hook implementations they want to share.

It is exactly as if the plugins had to declare their hook implementations as a list of names in a separate file, which would then be parsed by the main application, issuing a `dlsym` call for each name. However, since this approach is not taken and instead everything is detailed programmatically for convenience, (some of) the executable functions need to be exported (either with `-Wl,--export-dynamic` or with `-Wl,--dynamic-list`). Please be mindful of that when building an application other than the example provided.

## Usage

`cwedol.h` borrows the STB-like single header style, thus the two headers `cwedol.h` and `cwplugin.h` aren't meant to be compiled once for several projects but instead copy-pasted in each.

See `INSTALL.md` and the `example` folder for a working usage of cwedol.
