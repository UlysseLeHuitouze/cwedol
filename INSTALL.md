# How to run

You may want to run the example provided to further see the calling hierarchy happening.

To build, simply cd to the example folder, then run `make`. Finally, run `./main`.

You should then see these lines:

```
[Main app] Magic number: 3
[Plugin A] Loading hooks...
[Plugin B] Loading hooks...
[Plugin A] hello world
[Main app] ./libpluginA.so returned -10908503
[PluginB] hello world
[Main app] ./libpluginB.so returned 4
```
