//
// Created by Ulysse Le Huitouze on 9/1/23.
//

#ifndef CWEDOL_HELLO_PLUGIN_HOOK_H
#define CWEDOL_HELLO_PLUGIN_HOOK_H


typedef int (*hello_plugin_type)(int);

int hello_plugin(int magic);

#endif //CWEDOL_HELLO_PLUGIN_HOOK_H
