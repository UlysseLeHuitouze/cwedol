//
// Created by Ulysse Le Huitouze on 9/1/23.
//

#include <stdio.h>
#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif
#include "cwplugin.h"
#include "hello_plugin_hook.h"

int hello_plugin(int magic)
{
    printf("[PluginB] hello world\n");
    return magic + 1;
}

bool cwloadplugin()
{
    printf("[Plugin B] Loading hooks...\n");
    bool b = cwreghook(hello_plugin);
    if (!b)
        fprintf(
                stderr,
                "[Plugin B] failed to make the application load hello_plugin impl"
        );
    return b;
}