#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdint.h>

#define CWEDOL_IMPLEMENTATION
#include "cwedol.h"

// gives plugin entry point information which will load hook implementations
#include "cwplugin.h"
// gives plugin hook interface information
#include "hello_plugin_hook.h"


#define assert(cond, msg, ...) ({                 \
    if (!(cond)) {                                \
        fprintf(stderr, msg "\n", ##__VA_ARGS__); \
        exit(1);                                  \
    }                                             \
    {}                                            \
})



CW_DECLARE_HOOK_HANDLER(hello_plugin_hook_handler);


static void regPlugin(const char *filepath)
{
    cwsetpluginname(filepath);

    void *plugin = dlopen(filepath, RTLD_NOW | RTLD_LOCAL);
    assert(plugin != NULL, "[Main app] failed to load %s", filepath);

    cwloadplugin_t entry_point = (cwloadplugin_t)dlsym(plugin, CW_PLUGIN_ENTRY_POINT_NAME);
    assert(entry_point != NULL,
           "[Main app] failed to load cwloadplugin entry point for plugin %s", filepath);

    if (!entry_point())
        fprintf(
                stderr,
                "[Main app] Failed to load some hook impl in plugin %s",
                filepath
        );
}


int main() {
    int magic = 3;
    printf("[Main app] Magic number: %d\n", magic);

    // allocate space for hook handler
    CW_INIT_HOOK_HANDLER(hello_plugin_hook_handler, 2);
    // register ourselves to cwedol
    // we will be managing the implementations of the function hello_plugin (see hello_plugin_hook.h)
    cwreghookhandler("hello_plugin", &hello_plugin_hook_handler);

    // load both plugins and
    // their respective implementation of hello_plugin (by calling the plugins' entry point)
    regPlugin("./libpluginA.so");
    regPlugin("./libpluginB.so");


    // calling the hook implementations one by one
    for (int i = 0; i < 2; i++) {
        HookImpl hook = CW_GET_HOOK_IMPL(hello_plugin_hook_handler, i);
        printf("[Main app] %s returned %d\n",
               hook.plugin_name,
               ( (hello_plugin_type)hook.hook_ptr )(magic)  // actual call to the hook implementation
        );
    }

    // free hook handler space. can no longer add hook implementations to it.
    CW_FREE_HOOK_HANDLER(hello_plugin_hook_handler);
    return 0;
}
