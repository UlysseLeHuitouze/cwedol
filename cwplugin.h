//
// Created by Ulysse Le Huitouze on 9/1/23.
//

#ifndef CWEDOL_CWPLUGIN_H
#define CWEDOL_CWPLUGIN_H


#undef CWEDOL_IMPLEMENTATION
#include "cwedol.h"



//
// ==================== ABSTRACT ====================
//
// Here we define a few utilities mainly for plugins to use:
//
//
// - macro CW_PLUGIN_ENTRY_POINT_NAME :: char*
// - typedef cwloadplugin_t :: bool (*)()
// - func cwloadplugin :: bool
//
// See the example folder for a concrete usage of this design.
//
//
// ==================================================
//





/**
 * #cwloadplugin as a string.
 * Typical usage:
 * @code
 *  void *plugin = dlopen(...);
 *  cwloadplugin_t entryPoint = (cwloadplugin_t) dlsym(plugin, CW_PLUGIN_ENTRY_POINT_NAME);
 *
 *  if (!entryPoint()) {
 *  ...
 * @endcode
 */
#define CW_PLUGIN_ENTRY_POINT_NAME "cwloadplugin"


/**
 * The type of #cwloadplugin.
 *
 * Typical usage:
 * @code
 *  void *plugin = dlopen(...);
 *  cwloadplugin_t entryPoint = (cwloadplugin_t) dlsym(plugin, CW_PLUGIN_ENTRY_POINT_NAME);
 *
 *  if (!entryPoint()) {
 *  ...
 * @endcode
 */
typedef bool (*cwloadplugin_t)();


/**
 * Main entry point of plugins.
 * The plugins will implement it with calls to cwedol.h#cwreghook.
 * See the example folder for a concrete usage of this design.
 * @return whether the operation succeeded or not
 */
bool cwloadplugin();



#endif //CWEDOL_CWPLUGIN_H
